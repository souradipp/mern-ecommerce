const express = require('express');
const products = require('./data/products');
const dotenv = require('dotenv');
const app = express();

dotenv.config();

app.get("/api/products", (req,res) => {
    res.json(products).status(200);
});

app.get("/api/products/:id", (req,res) => {
    const product = products.find(el => el._id === req.params.id);
    res.json(product);
});

const PORT = process.env.PORT || 5000

app.listen(PORT, ()=> {console.log("server started")})