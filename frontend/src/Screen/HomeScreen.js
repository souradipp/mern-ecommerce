import React, { Fragment, useState, useEffect } from 'react'
import Product from '../components/Product'
import { Row, Col } from 'react-bootstrap';
import axios from 'axios';

const HomeScreen = () => {
    const [products, setProducts] = useState([]);

    useEffect(()=>{
        const fetchProduct = async ()=> {
            const {data} = await axios.get("/api/products");
            setProducts(data);
        }

        fetchProduct();
    },[]);

    return (
        <Fragment>
            <h1>Latest product</h1>
            <Row>
                {
                    products.map((product,i) => (
                        <Col key={product._id} sm={12} md={6} lg={4}>
                            <Product product={product}/>
                        </Col>
                    ))
                }
            </Row>
        </Fragment>
    )
}

export default HomeScreen
