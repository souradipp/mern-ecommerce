import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { Container } from 'react-bootstrap';
import Header from './components/Header';
import Footer from './components/Footer';


import HomeScreen from './Screen/HomeScreen'
import ProductScreen from './Screen/ProductScreen'


const App = () => {
  return (

    <Router>
      <Header />
      <main className="py-3">
        <Container>
          <Route component={HomeScreen} path="/" exact />
          <Route component={ProductScreen} path="/product/:id" />
        </Container>

      </main>
      <Footer />
    </Router>
  );
}

export default App;
