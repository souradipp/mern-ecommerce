import React from 'react'
import { Card } from 'react-bootstrap'
import {Link} from 'react-router-dom'

import Rating from "./Rating";

const Product = ({ product }) => {
    return (
        <Card className="my-3 p-3 rounded">
            <Link to={`/product/${product._id}`}>
                <Card.Img src={`${product.image}`}></Card.Img>
            </Link>

            <Card.Body>
                <Link to={`/product/${product._id}`}>
                    <Card.Title as = 'div'>
                        <strong>{product.name}</strong>
                    </Card.Title>
                </Link>
            </Card.Body>

            <Card.Text as = 'div'>
                <div className="mx-3"> 
                    <Rating value={product.rating} text = {`${product.numReviews} reviews` } color='red'/>
                </div>
            </Card.Text>

            <Card.Text as="h3">
                <div className="mx-3">
                    ${product.price}
                </div>
            </Card.Text>

        </Card>
    )
}

export default Product
